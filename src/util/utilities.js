export const normalizeName = (name) => {
  let words = name.split('-');
  words.forEach((word, index) => {
    words[index] = `${word[0].toUpperCase()}${word.slice(1)}`;
  });
  return words.join(' ');
};
