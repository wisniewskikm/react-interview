const state = {
  devices: [
    {
      type: 'lock',
      state: 'unlocked',
      last_updated_at: 1508386138,
      slug: 'apt-143-lock',
      interactions: []
    },
    {
      type: 'lock',
      state: 'locked',
      last_updated_at: 1508386138,
      slug: 'apt-145-lock',
      interactions: []
    },
    {
      type: 'lock',
      state: 'unlocked',
      last_updated_at: 1511932721036,
      slug: 'apt-147-lock',
      interactions: [
        {
          "state": "locked",
          "timestamp": 1511932721036
        },
        {
          "state": "unlocked",
          "timestamp": 1511932721100
        },
        {
          "state": "locked",
          "timestamp": 1511932721200
        },
        {
          "state": "unlocked",
          "timestamp": 1511932721300
        },
        {
          "state": "locked",
          "timestamp": 1511932721400
        },
        {
          "state": "unlocked",
          "timestamp": 1511932721500
        },
        {
          "state": "locked",
          "timestamp": 1511932721600
        },
        {
          "state": "unlocked",
          "timestamp": 1511932721700
        },
        {
          "state": "locked",
          "timestamp": 1511932721800
        },
        {
          "state": "unlocked",
          "timestamp": 1511932721900
        }
      ]
    },
    {
      type: 'thermostat',
      state: 'heating',
      last_updated_at: 1508386138,
      slug: 'apt-143-thermostat',
      interactions: []
    }
  ]
};

export default state;
