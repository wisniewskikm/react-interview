import React, { Component } from 'react';
import '../styles/App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import updateLock from '../redux/reducers';
import AboutUs from './AboutUs';
import Device from './Device';
import initialState from '../util/initialState.json'

let store = createStore(updateLock, initialState);

class App extends Component {
  render() {
    return (
      <div className="App">
        <Provider store={store}>
          <BrowserRouter>
            <div>
              <Switch>
                <Route exact path='/' component={AboutUs}/>
                <Route path='/about-us' component={AboutUs}/>
                <Route path='/device/:slug' component={Device}/>
                <Route component={NoMatch}/>
              </Switch>
            </div>
          </BrowserRouter>
        </Provider>
      </div>
    );
  }
};

const NoMatch = ({ location }) => (
  <div>
    <h3>Path <code>{location.pathname}</code> not found.</h3>
  </div>
);

export default App;
