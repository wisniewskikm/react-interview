import React from 'react';
import { connect } from 'react-redux';
import { normalizeName } from '../util/utilities';

const mapStateToProps = (state, ownProps) => {
  return {
    devices: state.devices.map(device => {
      return {
        name: normalizeName(device.slug),
        slug: device.slug
      }
    })
  }
};

class AboutUs extends React.Component {
  constructor(props) {
    super(props);
    this.devices = props.devices;
    this.state = {value: this.devices[0].slug};
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleClick(event) {
    this.props.history.push(`/device/${this.state.value}`);
  }

  render() {
    return (
      <div>
        <h1 className="jumbotron">CasaIQ - A Smart Home Solution Custom-built for Apartments, Condos, and Multifamily Real Estate.</h1>
        <select value={this.state.value} onChange={this.handleChange} className="btn btn-outline-dark">
          {this.devices.map(device => <option key={device.slug} value={device.slug}>{device.name}</option>)}
        </select>
        <button onClick={this.handleClick} className="btn btn-primary m-1">Go to Device</button>
      </div>
    )
  }
};

AboutUs = connect(mapStateToProps)(AboutUs);

export default AboutUs;
