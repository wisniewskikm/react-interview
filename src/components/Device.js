import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { lock, unlock } from '../redux/actions';
import { normalizeName } from '../util/utilities';

const mapStateToProps = (state, ownProps) => {
  return {
    device: state.devices.find((element) => {
      return ownProps.match.params.slug === element.slug;
    })
  }
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    clickLock: () => {
      dispatch(lock(ownProps.match.params.slug));
    },
    clickUnlock: () => {
      dispatch(unlock(ownProps.match.params.slug));
    }
  }
};

class Device extends React.Component {
  render () {
    const device = this.props.device;
    let content = null;
    if (device.type === 'lock') {
      content = <div className="text-left">
          <p>Lock Status: {device.state}</p>
          <p>Last Updated At: {new Date(device.last_updated_at).toString()}</p>
          <div className="text-center">
            <button onClick={this.props.clickLock} className="btn btn-primary m-1">Lock</button>
            <button onClick={this.props.clickUnlock} className="btn btn-primary m-1">Unlock</button>
          </div>
          <div>
            <h4>Recent Interactions</h4>
            <ul>
              {device.interactions.map(interaction =>
                <li key={interaction.timestamp}>state: {interaction.state}
                  <br/>timestamp: {new Date(interaction.timestamp).toString()}
                </li>
              )}
            </ul>
          </div>
        </div>;
    } else {
      content = <p>We are not supporting this device at the moment.</p>;
    }
    return (
      <div className="container">
        <div className="float-left p-1">
          <Link to="/about-us">Home</Link>
        </div>
        <h2 className="p-3">{normalizeName(device.slug)}</h2>
        {content}
      </div>
    );
  }
};

Device = connect(mapStateToProps, mapDispatchToProps)(Device);

export default Device;
