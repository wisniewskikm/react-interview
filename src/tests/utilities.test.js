import { normalizeName } from '../util/utilities';

it('should normalize the device name', () => {
  const newName = normalizeName('apt-143-lock');
  expect(newName).toBe('Apt 143 Lock');
});
