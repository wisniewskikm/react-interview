import React from 'react';
import { lock, unlock } from '../redux/actions';
import updateLock from '../redux/reducers';
import init from '../util/initialState.json';

let initialState = {};
beforeEach(() => {
  initialState = {
    devices: [
      {
        type: 'lock',
        state: 'unlocked',
        last_updated_at: 1508386138,
        slug: 'apt-143-lock',
        interactions: []
      }
    ]
  }
});

it('should lock the lock and update the interactions', () => {
  const newState = updateLock(initialState, lock('apt-143-lock'));
  expect(newState.devices[0].state).toBe('locked');
  expect(newState.devices[0].interactions.length).toEqual(1);
  expect(newState.devices[0].interactions[0].state).toBe('locked');
});

it('should unlock the lock and update the interactions', () => {
  initialState.devices[0].state = 'locked';
  const newState = updateLock(initialState, unlock('apt-143-lock'));
  expect(newState.devices[0].state).toBe('unlocked');
  expect(newState.devices[0].interactions.length).toEqual(1);
  expect(newState.devices[0].interactions[0].state).toBe('unlocked');
});

it('should update the lock and not delete other devices', () => {
  initialState = init;
  const numDevices = initialState.devices.length;
  const newState = updateLock(initialState, unlock('apt-143-lock'));
  expect(newState.devices.length).toEqual(numDevices);
  expect(newState.devices[1]).not.toBeUndefined();
});

it('should update the lock state and limit interactions to 10', () => {
  initialState.devices[0].interactions = [
    {
      "state": "locked",
      "timestamp": 1508383138
    },
    {
      "state": "unlocked",
      "timestamp": 1508385138
    },
    {
      "state": "locked",
      "timestamp": 1508383138
    },
    {
      "state": "unlocked",
      "timestamp": 1508385138
    },
    {
      "state": "locked",
      "timestamp": 1508383138
    },
    {
      "state": "unlocked",
      "timestamp": 1508385138
    },
    {
      "state": "locked",
      "timestamp": 1508383138
    },
    {
      "state": "unlocked",
      "timestamp": 1508385138
    },
    {
      "state": "locked",
      "timestamp": 1508383138
    },
    {
      "state": "unlocked",
      "timestamp": 1508385138
    }
  ];
  const newState = updateLock(initialState, lock('apt-143-lock'));
  expect(newState.devices[0].interactions.length).toEqual(10);
  expect(newState.devices[0].interactions[0].state).toBe('unlocked');
  expect(newState.devices[0].interactions[9].state).toBe('locked');
});
