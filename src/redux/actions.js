export const LOCK = 'LOCK';
export const UNLOCK = 'UNLOCK';

export function lock(slug) {
  return {
    type: LOCK,
    slug,
    updatedAt: Date.now()
  }
};

export function unlock(slug) {
  return {
    type: UNLOCK,
    slug,
    updatedAt: Date.now()
  }
};
