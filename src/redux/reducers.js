import { LOCK, UNLOCK } from './actions';

const initialState = {
  devices: []
};

const toggleLock = (state, action, newLockState) => {
  return Object.assign({}, state, {
    devices: state.devices.map((device, index) => {
      if (device.slug === action.slug) {
        if(newLockState === device.state) {
          // trying to lock a locked device or vice versa, do nothing
          return device;
        }
        let newInteractions = JSON.parse(JSON.stringify(device.interactions));
        newInteractions.push({
          state: newLockState,
          timestamp: action.updatedAt
        });
        if (newInteractions.length > 10) {
          // treat interactions as a queue, FIFO
          newInteractions.shift();
        }
        return Object.assign({}, device, {
          state: newLockState,
          last_updated_at: action.updatedAt,
          interactions: newInteractions
        })
      } else {
        // if this isn't the device we want, we can just return it
        return device;
      }
    })
  });
};

const updateLock = (state = initialState, action) => {
  let returnState = state;
  switch (action.type) {
    case LOCK:
      returnState = toggleLock(state, action, 'locked');
      break;
    case UNLOCK:
      returnState = toggleLock(state, action, 'unlocked');
      break;
    default:
      return state;
  }
  return returnState;
};

export default updateLock;
